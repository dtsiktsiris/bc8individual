
package entities;

import java.time.LocalDateTime;

public class Assignment {
    private int id;
    private String title;
    private String description;
    private LocalDateTime subDateTime;
    private Course course;

    public Assignment() {
    }

    public Assignment(int id, String title, String description, LocalDateTime subDateTime) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.subDateTime = subDateTime;
    }

    public Assignment(int id, String title, String description, LocalDateTime subDateTime, Course course) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.subDateTime = subDateTime;
        this.course = course;
    }

    public int getId() {
        return id;
    }
    

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getSubDateTime() {
        return subDateTime;
    }

    public void setSubDateTime(LocalDateTime subDateTime) {
        this.subDateTime = subDateTime;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public String toString() {
        return "Assignment{" + "title=" + title + ", description=" + description + ", subDateTime=" + subDateTime + ", \n" + course + '}';
    }
}
