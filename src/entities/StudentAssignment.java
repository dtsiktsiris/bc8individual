package entities;

import entities.Student;
import entities.Assignment;
import java.time.LocalDate;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;

public class StudentAssignment {
    private int id;
    private int oralMark;
    private int totalMark;
    private Student student;
    private Assignment assignment;

    public StudentAssignment() {
    }

    public StudentAssignment(Student student, Assignment assignment) {
        this.student = student;
        this.assignment = assignment;
    }

    public StudentAssignment(int id, int oralMark, int totalMark, Student student, Assignment assignment) {
        this.id = id;
        this.oralMark = oralMark;
        this.totalMark = totalMark;
        this.student = student;
        this.assignment = assignment;
    }

    public int getId() {
        return id;
    }

    public int getOralMark() {
        return oralMark;
    }

    public void setOralMark(int oralMark) {
        this.oralMark = oralMark;
    }

    public int getTotalMark() {
        return totalMark;
    }

    public void setTotalMark(int totalMark) {
        this.totalMark = totalMark;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Assignment getAssignment() {
        return assignment;
    }

    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    

//    public static void showStudentsAssignmentsByDate(LocalDate ld, StudentAssignment[] studentAssignments) {
//        System.out.println();
//        int weekNumber = ld.get(WeekFields.ISO.weekOfWeekBasedYear());
//        boolean found = false;
//        for (StudentAssignment sa : studentAssignments) {
//            for (Assignment assignment : sa.getAssignments()) {
//                if (weekNumber == assignment.getSubDateTime().get(WeekFields.ISO.weekOfWeekBasedYear())) {
//                    System.out.println(sa.getStudent());
//                    found=true;
//                }
//            }
//        }
//        if (!found){
//            System.out.println("Submission dates not found");
//        }
//    }

}
