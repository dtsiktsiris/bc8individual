
package entities;

import java.time.LocalDate;
import entities.persons.Person;

public class Student extends Person{
    
    private LocalDate dateOfBirth;
    private double tuitionFees;

    public Student() {
        super();
    }

    public Student(int id, String firstName, String lastName, LocalDate dateOfBirth, double tuitionFees) {
        super(id, firstName, lastName);
        this.dateOfBirth = dateOfBirth;
        this.tuitionFees = tuitionFees;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public double getTuitionFees() {
        return tuitionFees;
    }

    public void setTuitionFees(double tuitionFees) {
        this.tuitionFees = tuitionFees;
    }

    @Override
    public String toString() {
        return "Student{" + "id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", dateOfBirth=" + dateOfBirth + ", tuitionFees=" + tuitionFees + '}';
    }
    
    
}
