
package entities;

import entities.persons.Person;

public class Trainer extends Person{
    private String subject;

    public Trainer() {
        super();
    }
    
    public Trainer(int id, String firstName, String lastName, String subject) {
        super(id, firstName, lastName);
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Trainer{" + "firstName=" + firstName + ", lastName=" + lastName + ", subject=" + subject + '}';
    }
    
    
}
