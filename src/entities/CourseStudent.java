package entities;

import java.util.HashMap;
import java.util.Map;

public class CourseStudent {
    private int id;
    private Course course;
    private Student student;

    public CourseStudent() {
    }

    public CourseStudent(int id, Course course, Student student) {
        this.id = id;
        this.course = course;
        this.student = student;
    }

    public int getId() {
        return id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public static void moreThanOneCourse(CourseStudent[] coursesStudents) {
        System.out.println();
        System.out.println();
        System.out.println("Students who has more than one course:");
        Map<Integer, Integer> count = new HashMap();
        boolean checkIfAnyone = false;
        for (CourseStudent cs : coursesStudents) {
                if (count.containsKey(cs.getStudent().getId())) {
                    count.put(cs.getStudent().getId(), count.get(cs.getStudent().getId()) + 1);
                    checkIfAnyone = true;
                } else {
                    count.put(cs.getStudent().getId(), 1);
                }
        }
        if (checkIfAnyone) {
            for (Map.Entry<Integer, Integer> entry : count.entrySet()) {
                if (entry.getValue() > 1) {
                    System.out.println("Student with id: "+entry.getKey());
                }
            }
        } else {
            System.out.println("Not any user has more than one courses");
        }
    }
}
