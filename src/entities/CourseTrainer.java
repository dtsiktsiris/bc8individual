
package entities;

import entities.Trainer;
import entities.Course;
import java.util.ArrayList;
import java.util.List;

public class CourseTrainer {
    private int id;
    private double payment;
    private Course course;
    private Trainer trainer;

    public CourseTrainer() {
    }

    public CourseTrainer(int id, double payment, Course course, Trainer trainer) {
        this.id = id;
        this.payment = payment;
        this.course = course;
        this.trainer = trainer;
    }

    public int getId() {
        return id;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
    
    
    
}
