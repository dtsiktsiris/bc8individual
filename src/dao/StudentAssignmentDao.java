
package dao ;
//table exams

import entities.StudentAssignment;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentAssignmentDao extends Dao{
    
    public StudentAssignment[] getAllStudentAssignment() {
        List<StudentAssignment> tempStudentAsignment = new ArrayList();
        AssignmentDao ad = new AssignmentDao();
        StudentDao sd = new StudentDao();
        StudentAssignment[] studentsAssignments = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Exams");
            
            while (rs.next()) {
                
                tempStudentAsignment.add(new StudentAssignment(rs.getInt(1),rs.getInt(2),rs.getInt(3),
                        sd.findStudentById(rs.getInt(4)),ad.findAssignmentById(rs.getInt(5))));
                
            }
            closeConnections(stmt, rs);
            studentsAssignments = new StudentAssignment[tempStudentAsignment.size()];
            studentsAssignments = tempStudentAsignment.toArray(studentsAssignments);
        } catch (SQLException ex) {
            Logger.getLogger(StudentAssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return studentsAssignments;
    }
    
    public boolean checkEntryIfExist(int sID, int aID){
        boolean exist = false;
        
        try {
            PreparedStatement pst = getConnection().prepareStatement("SELECT * FROM Exams WHERE student_id = ? AND assignment_id = ?");
            pst.setInt(1, sID);
            pst.setInt(2, aID);
            ResultSet rs = pst.executeQuery();
            exist = rs.next();
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(StudentAssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return exist;
    }
    
    
    public boolean insertExamToDb(int sID, int aID, int oralMark, int totalMark){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Exams (student_id, assignment_id, oral_mark, total_mark) VALUES (?, ?, ?, ?)");
            pst.setInt(1, sID);
            pst.setInt(2, aID);
            pst.setInt(3, oralMark);
            pst.setInt(4, totalMark);
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(StudentAssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
}
