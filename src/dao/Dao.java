package dao;

import entities.Trainer;
import entities.Student;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Dao {

//    private static Dao manageDb;

    private static final String DB_URL = "jdbc:mysql://localhost:3306/bc8individual?serverTimezone=Europe/Athens";

    private static final String USER = "root";
    private static final String PASS = "root";
    
    private static Connection con = null;
//
//    public static Dao getInstance() {
//        if (manageDb == null) {
//            manageDb = new Dao();
//        }
//        return manageDb;
//    }

    public Dao() {
    }

    protected Connection getConnection() {
        
        try {
            con = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    }

    protected void closeConnections(Statement st, ResultSet rs) {
        try {
            rs.close();
            closeConnections(st);
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    protected void closeConnections(Statement st) {
        try {
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
