
package dao;

import entities.Trainer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TrainerDao extends Dao{
    
    public Trainer[] getAllTrainers() {
        List<Trainer> tempTrainers = new ArrayList();
        Trainer[] trainers = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Trainers");
            while (rs.next()) {
                tempTrainers.add(new Trainer(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4)));
            }
            closeConnections(stmt, rs);
            trainers = new Trainer[tempTrainers.size()];
            trainers = tempTrainers.toArray(trainers);
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return trainers;
    }
    
    
    public boolean insertTrainerToDb(Trainer t){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Trainers (first_name, last_name, subject) VALUES (?, ?, ?)");
            pst.setString(1, t.getFirstName());
            pst.setString(2, t.getLastName());
            pst.setString(3, t.getSubject());
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
    public Trainer findTrainerById(int id) {
        Trainer t = null;

        try {
            PreparedStatement pst = getConnection().prepareStatement("select * from Trainers where trainer_id = ?");
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            rs.next();
            t = new Trainer(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
            closeConnections(pst, rs);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return t;
    }
    
}
