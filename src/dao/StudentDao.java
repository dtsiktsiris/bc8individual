package dao;

import entities.Student;
import individualproject.Helper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudentDao extends Dao {

    public Student[] getAllStudents() {
        List<Student> tempStudents = new ArrayList();
        Student[] students = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Students");//gia insert executedUpdate
            while (rs.next()) {

                //System.out.println( rs.getString(6) + "  " + rs.getString(7));
                tempStudents.add(new Student(rs.getInt(1), rs.getString(2), rs.getString(3),
                        LocalDate.parse(rs.getString(4), Helper.dbDateFormat), rs.getDouble(5)));

            }
            closeConnections(stmt, rs);
            students = new Student[tempStudents.size()];
            students = tempStudents.toArray(students);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        return students;
    }
    
    public void getStudentsMoreThanOneCourse() {
        Student[] students = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT s.student_id,s.last_name, s.first_name, COUNT(cl.student_id) as classSum FROM bc8individual.Classes as cl, bc8individual.Students as s\n" +
                                              " WHERE cl.student_id=s.student_id GROUP BY cl.student_id HAVING classSum>1");
            while (rs.next()) {

                System.out.println(rs.getInt(1)+" "+ rs.getString(2)+" "+ rs.getString(3));
                
            }
            closeConnections(stmt, rs);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
    
    public boolean insertStudentToDb(Student s){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Students (first_name, last_name, date_of_birth, tuition_fees) VALUES (?, ?, ?, ?)");
            pst.setString(1, s.getFirstName());
            pst.setString(2, s.getLastName());
            pst.setDate(3, java.sql.Date.valueOf(s.getDateOfBirth()));
            pst.setDouble(4, s.getTuitionFees());
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
    public Student findStudentById(int id) {
        Student s = null;

        try {
            PreparedStatement pst = getConnection().prepareStatement("select * from Students where student_id = ?");
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            rs.next();
            s = new Student(rs.getInt(1), rs.getString(2), rs.getString(3),
                    LocalDate.parse(rs.getString(4), Helper.dbDateFormat), rs.getDouble(5));
            closeConnections(pst, rs);
        } catch (SQLException ex) {
            Logger.getLogger(StudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return s;
    }

}
