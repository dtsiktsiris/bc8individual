
package dao;

import entities.Assignment;
import individualproject.Helper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AssignmentDao extends Dao {
    
    public Assignment[] getAllAssignments() {
        CourseDao cd = new CourseDao();
        List<Assignment> tempAssignmentss = new ArrayList();
        Assignment[] assignments = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Assignments");
            while (rs.next()) {
                
                tempAssignmentss.add(new Assignment(rs.getInt(1),rs.getString(2),rs.getString(3),
                        LocalDateTime.parse(rs.getString(4),Helper.dbDateTimeFormat),cd.findCourseById(rs.getInt(5))));
                
            }
            closeConnections(stmt, rs);
            assignments = new Assignment[tempAssignmentss.size()];
            assignments = tempAssignmentss.toArray(assignments);
        } catch (SQLException ex) {
            Logger.getLogger(AssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return assignments;
    }
    
    public Assignment[] getAssignmentsAvailableForStudent(int sID){
        
        List<Assignment> tempAssignmentss = new ArrayList();
        Assignment[] assignments = null;
        try {
            PreparedStatement pst = getConnection().prepareStatement("SELECT assignment_id,title,description,sub_date_time FROM students as s, classes as cl, assignments as a \n" +
                                                "where s.student_id= cl.student_id and cl.course_id = a.course_id and s.student_id=?");
            pst.setInt(1, sID);
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
                
                tempAssignmentss.add(new Assignment(rs.getInt(1),rs.getString(2),rs.getString(3),
                        LocalDateTime.parse(rs.getString(4),Helper.dbDateTimeFormat)));
                
            }
            closeConnections(pst, rs);
            assignments = new Assignment[tempAssignmentss.size()];
            assignments = tempAssignmentss.toArray(assignments);
        } catch (SQLException ex) {
            Logger.getLogger(AssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return assignments;
    }
    
    public boolean insertAssignmentToDb(Assignment a){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Assignments (title, description, sub_date_time, course_id) VALUES (?, ?, ?, ?)");
            pst.setString(1, a.getTitle());
            pst.setString(2, a.getDescription());
            pst.setTimestamp(3, java.sql.Timestamp.valueOf(a.getSubDateTime()));
            pst.setInt(4, a.getCourse().getId());
            pst.executeUpdate();
            pst.close();
            closeConnections(pst);
            insertOk = true;
        } catch (SQLException ex) {
            Logger.getLogger(AssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
    public Assignment findAssignmentById(int id){
        Assignment a =null;
        CourseDao cd = new CourseDao();
        try {
            
            PreparedStatement pst = getConnection().prepareStatement("select * from Assignments where assignment_id = ?");
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            rs.next();
            a = new Assignment(rs.getInt(1),rs.getString(2),rs.getString(3),
                        LocalDateTime.parse(rs.getString(4),Helper.dbDateTimeFormat),cd.findCourseById(rs.getInt(5)));
            closeConnections(pst, rs);
        } catch (SQLException ex) {
            Logger.getLogger(AssignmentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return a;
    }
    
}
