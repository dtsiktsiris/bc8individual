
package dao;

import entities.CourseStudent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CourseStudentDao extends Dao {
    
    public CourseStudent[] getAllCourseStudent() {
        List<CourseStudent> tempCourseStudent = new ArrayList();
        CourseDao cd = new CourseDao();
        StudentDao sd = new StudentDao();
        CourseStudent[] coursesStudents = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Classes");
            while (rs.next()) {
                
                tempCourseStudent.add(new CourseStudent(rs.getInt(1),cd.findCourseById(rs.getInt(2)),sd.findStudentById(rs.getInt(3))));
                
            }
            closeConnections(stmt, rs);
            coursesStudents = new CourseStudent[tempCourseStudent.size()];
            coursesStudents = tempCourseStudent.toArray(coursesStudents);
        } catch (SQLException ex) {
            Logger.getLogger(CourseStudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return coursesStudents;
    }
        
    public boolean checkEntryIfExist(int sID, int cID){
        boolean exist = false;
        
        try {
            PreparedStatement pst = getConnection().prepareStatement("SELECT * FROM Classes WHERE student_id = ? AND course_id = ?");
            pst.setInt(1, sID);
            pst.setInt(2, cID);
            ResultSet rs = pst.executeQuery();
            exist = rs.next();
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseStudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return exist;
    }
    
    public boolean insertClassToDb(int sID, int cID){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Classes (student_id, course_id) VALUES (?, ?)");
            pst.setInt(1, sID);
            pst.setInt(2, cID);
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseStudentDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
}
