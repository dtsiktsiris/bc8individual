
package dao;

import entities.CourseTrainer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CourseTrainerDao extends Dao {
    
    public CourseTrainer[] getAllCourseTrainer() {
        List<CourseTrainer> tempCourseTrainer = new ArrayList();
        CourseDao cd = new CourseDao();
        TrainerDao td = new TrainerDao();
        CourseTrainer[] coursesTrainers = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Instructs");
            while (rs.next()) {
                
                tempCourseTrainer.add(new CourseTrainer(rs.getInt(1),rs.getDouble(2),cd.findCourseById(rs.getInt(3)),td.findTrainerById(rs.getInt(4))));
                
            }
            closeConnections(stmt, rs);
            coursesTrainers = new CourseTrainer[tempCourseTrainer.size()];
            coursesTrainers = tempCourseTrainer.toArray(coursesTrainers);
        } catch (SQLException ex) {
            Logger.getLogger(CourseTrainerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return coursesTrainers;
    }
    
    public boolean checkEntryIfExist(int tID, int cID){
        boolean exist = false;
        
        try {
            PreparedStatement pst = getConnection().prepareStatement("SELECT * FROM Instructs WHERE trainer_id = ? AND course_id = ?");
            pst.setInt(1, tID);
            pst.setInt(2, cID);
            ResultSet rs = pst.executeQuery();
            exist = rs.next();
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseTrainerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return exist;
    }
    
    public boolean insertInstructToDb(int tID, int cID, double payment){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Instructs (trainer_id, course_id, payment) VALUES (?, ?, ?)");
            pst.setInt(1, tID);
            pst.setInt(2, cID);
            pst.setDouble(3, payment);
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseTrainerDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
}
