
package dao;

import entities.Course;
import individualproject.Helper;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CourseDao extends Dao{
    
    public Course[] getAllCourses() {
        List<Course> tempCourse = new ArrayList();
        Course[] courses = null;
        try {
            Statement stmt = getConnection().createStatement();
            ResultSet rs = stmt.executeQuery("select * from Courses");
            while (rs.next()) {
                
                tempCourse.add(new Course(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                        LocalDate.parse(rs.getString(5),Helper.dbDateFormat),LocalDate.parse(rs.getString(6),Helper.dbDateFormat),rs.getDouble(7)));
                
            }
            closeConnections(stmt, rs);
            courses = new Course[tempCourse.size()];
            courses = tempCourse.toArray(courses);
        } catch (SQLException ex) {
            Logger.getLogger(CourseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return courses;
    }
    
    public boolean insertCourseToDb(Course c){
        boolean insertOk = false;
        try {
            PreparedStatement pst = getConnection().prepareStatement("INSERT INTO Courses (title, stream, type, start_date, end_date, fee) VALUES (?, ?, ?, ?, ?, ?)");
            pst.setString(1, c.getTitle());
            pst.setString(2, c.getStream());
            pst.setString(3, c.getType());
            pst.setDate(4, java.sql.Date.valueOf(c.getStart_date()));
            pst.setDate(5, java.sql.Date.valueOf(c.getEnd_date()));
            pst.setDouble(6, c.getFee());
            pst.executeUpdate();
            pst.close();
            insertOk = true;
            closeConnections(pst);
        } catch (SQLException ex) {
            Logger.getLogger(CourseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return insertOk;
    }
    public Course findCourseById(int id){
        Course c =null;
        
        try {
            
            PreparedStatement pst = getConnection().prepareStatement("select * from Courses where course_id = ?");
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            rs.next();
            c = new Course(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                        LocalDate.parse(rs.getString(5),Helper.dbDateFormat),LocalDate.parse(rs.getString(6),Helper.dbDateFormat),rs.getDouble(7));
            closeConnections(pst, rs);
        } catch (SQLException ex) {
            Logger.getLogger(CourseDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return c;
    }
}
