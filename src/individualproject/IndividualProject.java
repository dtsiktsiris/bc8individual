package individualproject;

import entities.CourseStudent;
import dao.AssignmentDao;
import dao.CourseDao;
import dao.CourseStudentDao;
import dao.CourseTrainerDao;
import dao.StudentAssignmentDao;
import dao.StudentDao;
import dao.TrainerDao;
import java.util.Scanner;

public class IndividualProject {

    public static void main(String[] args) {
        
        CourseDao cd = new CourseDao();
        StudentDao sd = new StudentDao();
        TrainerDao td = new TrainerDao();
        AssignmentDao ad = new AssignmentDao();
        CourseStudentDao csd = new CourseStudentDao();
        StudentAssignmentDao sad = new StudentAssignmentDao();
        CourseTrainerDao ctd = new CourseTrainerDao();
        
        Scanner input = new Scanner(System.in);
        
        int choice = -1;

        boolean appExit = false;
        boolean editExit = true;
        boolean printsExit = true;

        if (choice == 0) {
            appExit = true;
        }
        while (!appExit) {
            UIPrints.mainMenu();
            choice = Helper.importDataChoice(input, 2);
            switch (choice) {
                case 1:
                    printsExit = false;
                    break;
                case 2:
                    editExit = false;
                    break;
                case 0:
                    appExit = true;
            }
            while (!editExit) {
                UIPrints.editMenu();
                choice = Helper.importDataChoice(input, 7);
                switch (choice) {
                    case 1:
                        ManualInput.coursesInput(input);
                        break;
                    case 2:
                        ManualInput.studentsInput(input);
                        break;
                    case 3:
                        ManualInput.trainersInput(input);
                        break;
                    case 4:
                        ManualInput.assignmentsInput(input);
                        break;
                    case 5:
                        ManualInput.courseStudentRel(cd.getAllCourses(), sd.getAllStudents());
                        break;
                    case 6:
                        ManualInput.courseTrainerRel(td.getAllTrainers(), cd.getAllCourses());
                        break;
                    case 7:
                        ManualInput.StudentAssignmentRel(sd.getAllStudents());
                        break;
                    case 0:
                        editExit = true;
                }
            }
            while (!printsExit) {
                UIPrints.PrintsMenu();
                choice = Helper.importDataChoice(input, 11);
                switch (choice) {
                    case 0:
                        printsExit = true;
                        break;
                    case 1:
                        UIPrints.printStudentData(sd.getAllStudents());
                        break;
                    case 2:
                        UIPrints.printCourseData(cd.getAllCourses());
                        break;
                    case 3:
                        UIPrints.printTrainerData(td.getAllTrainers());
                        break;
                    case 4:
                        UIPrints.printAssignmentData(ad.getAllAssignments());
                        break;
                    case 5:
                        UIPrints.printStudentsAssignmentsRelations(sad.getAllStudentAssignment());
                        break;
                    case 6:
                        UIPrints.printCoursesStudentsRelations(csd.getAllCourseStudent());
                        break;
                    case 7:
                        UIPrints.printCourseTrainerRelations(ctd.getAllCourseTrainer());
                        break;
                    case 8:
                        UIPrints.printAssignmentCourseRelations(ad.getAllAssignments());
                        break;
                    case 9:
                        CourseStudent.moreThanOneCourse(csd.getAllCourseStudent());
                        break;
                    case 10:
                        sd.getStudentsMoreThanOneCourse();
                        break;
                    case 11:
                        UIPrints.printStudentsAssignmentsCourseRelations(sad.getAllStudentAssignment());
                        break;
                }
            }
        }

        input.close();
        System.out.println();
        System.out.println("Application terminated");

    }

}
