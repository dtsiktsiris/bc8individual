package individualproject;

import entities.CourseTrainer;
import entities.StudentAssignment;
import entities.CourseStudent;
import entities.Trainer;
import entities.Student;
import entities.Assignment;
import entities.Course;

public class UIPrints {
    
    public static final void mainMenu() {
        System.out.println();
        System.out.println("What do you want to do? choose number");
        System.out.println("Prints (1)");
        System.out.println("Edit Database (2)");
        System.out.println("Exit Application (0)");
    }

    public static final void editMenu() {
        System.out.println();
        System.out.println("What do you want to Edit? choose number");
        System.out.println("Insert Course (1)");
        System.out.println("Insert Student (2)");
        System.out.println("Insert Trainer (3)");
        System.out.println("Insert Assignment (4)");
        System.out.println("Set relation between Student Course (5)");
        System.out.println("Set relation between Trainer Course (6)");
        System.out.println("Set relation between Student Assignment (7)");
        System.out.println("Exit Edits (0)");
    }

    public static final void PrintsMenu() {

        System.out.println();
        System.out.println("What do you want to Print? choose number");
        System.out.println("Print Students (1)");
        System.out.println("Print Courses (2)");
        System.out.println("Print Trainers (3)");
        System.out.println("Print Assignments (4)");
        System.out.println("Print Students Assignments (5)");
        System.out.println("Print Courses Students (6)");
        System.out.println("Print Courses Trainers (7)");
        System.out.println("Print Courses Assignments (8)");
        System.out.println("Print Students has more than one Course using PART A's function (9)");
        System.out.println("Print Students has more than one Course (10)");
        System.out.println("Print Students assignments Courses (11)");
        System.out.println("Exit Prints (0)");
    }

    public static void printCourseTrainerRelations(CourseTrainer[] coursesTrainers) {
        System.out.println();
        System.out.println();
        System.out.println("****** Course Trainer Relationships ******");
        for (CourseTrainer courseTrainer : coursesTrainers) {
            System.out.println();
            System.out.println(courseTrainer.getCourse().toString());
            System.out.println(courseTrainer.getTrainer().toString());
            System.out.println("Payment is: "+ courseTrainer.getPayment());
            
        }
    }

    public static void printStudentsAssignmentsRelations(StudentAssignment[] studentsAssignments) {
        System.out.println();
        System.out.println();
        System.out.println("****** Course Student Relationships ******");
        for (StudentAssignment studentAssignment : studentsAssignments) {
            System.out.println();
            System.out.println(studentAssignment.getStudent().toString());
            System.out.println("Assignment id: "+ studentAssignment.getAssignment().getId()+", Title: "+ studentAssignment.getAssignment().getTitle()+
                    ", Description: "+ studentAssignment.getAssignment().getDescription()+", Sub DateTime: "+ studentAssignment.getAssignment().getSubDateTime());
            System.out.println("Oral mark: "+studentAssignment.getOralMark()+", Total mark: "+studentAssignment.getTotalMark());
        }
    }
    public static void printStudentsAssignmentsCourseRelations(StudentAssignment[] studentsAssignments) {
        System.out.println();
        System.out.println();
        System.out.println("****** Course Student Relationships ******");
        for (StudentAssignment studentAssignment : studentsAssignments) {
            System.out.println();
            System.out.println(studentAssignment.getStudent().toString());
            System.out.println(studentAssignment.getAssignment().toString());
            System.out.println("Oral mark: "+studentAssignment.getOralMark()+", Total mark: "+studentAssignment.getTotalMark());
        }
    }

    public static void printCoursesStudentsRelations(CourseStudent[] coursesStudents) {
        System.out.println();
        System.out.println();
        System.out.println("****** Course Student Relationships ******");
        for (CourseStudent courseStudent : coursesStudents) {
            System.out.println();
            System.out.println(courseStudent.getCourse().toString());
            System.out.println(courseStudent.getStudent().toString());
        }
    }

    public static void printTrainerData(Trainer[] trainers) {
        System.out.println();
        System.out.println("****** Print Trainers ******");
        for (Trainer trainer : trainers) {
            System.out.println(trainer);
        }
    }

    public static void printStudentData(Student[] students) {

        System.out.println();
        System.out.println("****** Print Students ******");
        for (Student student : students) {
            System.out.println(student);
        }
    }
public static void printAssignmentCourseRelations(Assignment[] assignments) {
        System.out.println();
        System.out.println("****** Print Assignments Courses Relationships ******");
        for (Assignment assignment : assignments) {
            System.out.println(assignment);
        }
    }
    public static void printAssignmentData(Assignment[] assignments) {
        System.out.println();
        System.out.println("****** Print Assignments ******");
        for (Assignment assignment : assignments) {
            System.out.println("Assignment id: "+assignment.getId()+", "+ assignment.getTitle()+", "+assignment.getDescription()+
                    ", "+assignment.getSubDateTime());
        }
    }

    public static void printCourseData(Course[] courses) {

        System.out.println();
        System.out.println("****** Print Courses ******");
        for (Course course : courses) {
            System.out.println(course);
        }
    }
}
