package individualproject;

import dao.AssignmentDao;
import dao.CourseDao;
import dao.CourseStudentDao;
import dao.CourseTrainerDao;
import dao.StudentAssignmentDao;
import dao.StudentDao;
import dao.TrainerDao;
import entities.CourseStudent;
import entities.Trainer;
import entities.Student;
import entities.Assignment;
import entities.Course;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ManualInput {

    public static void assignmentsInput(Scanner input) {
        Assignment[] assignments = null;
        AssignmentDao ad = new AssignmentDao();
        CourseDao cd = new CourseDao();

        System.out.println("Assignment Input");
        boolean assignmentsOk = false;
        while (!assignmentsOk) {
            Assignment assignment = new Assignment();
            boolean inputOk = false;
            while (!inputOk) {
                System.out.println("Give title: ");
                String title = input.nextLine().trim();
                inputOk = Helper.checkPhrase(title);
                if (inputOk) {
                    assignment.setTitle(title);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give description: ");
                String description = input.nextLine().trim();
                inputOk = Helper.checkPhrase(description);
                if (inputOk) {
                    assignment.setDescription(description);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give subDateTime in this form \"17/03/2019 23:07:00\": ");
                String subDateTime = input.nextLine().trim();
                inputOk = Helper.checkAfterDateTime(subDateTime);
                if (inputOk) {
                    assignment.setSubDateTime(LocalDateTime.parse(subDateTime, Helper.dateTimeFormat));
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Choose the ID of course which is addressed: ");
                List<Integer> courseIds = new ArrayList();
                for (Course c : cd.getAllCourses()) {
                    System.out.println(c);
                    courseIds.add(c.getId());
                }
                String courseId = input.nextLine().trim();
                inputOk = (Helper.checkInt(courseId) && courseIds.contains(Integer.parseInt(courseId)));//elenxoume an einai noumero kai uparkto id
                if (inputOk) {
                    assignment.setCourse(cd.findCourseById(Integer.parseInt(courseId)));
                } else {
                    System.out.println("*Error:Please give an existing id!*");
                }
            }

            ad.insertAssignmentToDb(assignment);
            System.out.println("Do you want to add more assignments?y/n");
            String moreAssignments = input.nextLine();
            if (!moreAssignments.equals("y")) {
                assignmentsOk = true;
            }

        }

    }

    public static void coursesInput(Scanner input) {
        Course[] courses = null;

        System.out.println("Course Input");
        boolean coursesOk = false;
        while (!coursesOk) {
            Course course = new Course();
            boolean inputOk = false;
            while (!inputOk) {
                System.out.println("Give title: ");
                String title = input.nextLine().trim();
                inputOk = Helper.checkPhrase(title);
                if (inputOk) {
                    course.setTitle(title);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give stream: ");
                String stream = input.nextLine().trim();
                inputOk = Helper.checkPhrase(stream);
                if (inputOk) {
                    course.setStream(stream);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give type: ");
                String type = input.nextLine().trim();
                inputOk = Helper.checkPhrase(type);
                if (inputOk) {
                    course.setType(type);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give start date in this form \"17/03/2019\": ");
                String start_date = input.nextLine().trim();
                inputOk = Helper.checkAfterDate(start_date);
                if (inputOk) {
                    course.setStart_date(LocalDate.parse(start_date, Helper.dateFormat));
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give end date in this form \"17/03/2019\": ");
                String end_date = input.nextLine().trim();
                inputOk = Helper.checkAfterDate(end_date, course.getStart_date());
                if (inputOk) {
                    course.setEnd_date(LocalDate.parse(end_date, Helper.dateFormat));
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give Fees: ");
                String fee = input.nextLine().trim();
                inputOk = Helper.checkDouble(fee);
                if (inputOk) {
                    course.setFee(Double.parseDouble(fee));
                }
            }
            CourseDao cd = new CourseDao();
            cd.insertCourseToDb(course);
            System.out.println("Do you want to add more courses?y/n");
            String moreCourses = input.nextLine();
            if (!moreCourses.equals("y")) {
                coursesOk = true;
            }

        }

    }

    public static void studentsInput(Scanner input) {
        Student[] students = null;
        StudentDao sd = new StudentDao();
        System.out.println("Student Input");
        boolean studentsOk = false;
        while (!studentsOk) {
            Student student = new Student();
            boolean inputOk = false;
            while (!inputOk) {
                System.out.println("Give name: ");
                String name = input.nextLine().trim();
                inputOk = Helper.checkWord(name);
                if (inputOk) {
                    student.setFirstName(name);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give lastname: ");
                String lastname = input.nextLine().trim();
                inputOk = Helper.checkWord(lastname);
                if (inputOk) {
                    student.setLastName(lastname);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give Date of Birth in this form \"17/03/2019\": ");
                String bday = input.nextLine().trim();
                inputOk = Helper.checkBeforeDate(bday);
                if (inputOk) {
                    student.setDateOfBirth(LocalDate.parse(bday, Helper.dateFormat));
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give Fees: ");
                String tuitionFees = input.nextLine().trim();
                inputOk = Helper.checkDouble(tuitionFees);
                if (inputOk) {
                    student.setTuitionFees(Double.parseDouble(tuitionFees));
                }
            }
            sd.insertStudentToDb(student);
            System.out.println("Do you want to add more students?y/n");
            String moreStudents = input.nextLine();
            if (!moreStudents.equals("y")) {
                studentsOk = true;
            }
        }
    }

    public static void trainersInput(Scanner input) {
        Trainer[] trainers = null;
        TrainerDao td = new TrainerDao();
        System.out.println();
        System.out.println("Trainer Input");
        boolean trainersOk = false;
        while (!trainersOk) {
            Trainer trainer = new Trainer();
            boolean inputOk = false;
            while (!inputOk) {
                System.out.println("Give name: ");
                String name = input.nextLine().trim();
                inputOk = Helper.checkWord(name);
                if (inputOk) {
                    trainer.setFirstName(name);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give lastname: ");
                String lastname = input.nextLine().trim();
                inputOk = Helper.checkWord(lastname);
                if (inputOk) {
                    trainer.setLastName(lastname);
                }
            }
            inputOk = false;
            while (!inputOk) {
                System.out.println("Give subject: ");
                String subject = input.nextLine();
                inputOk = Helper.checkPhrase(subject);
                if (inputOk) {
                    trainer.setSubject(subject);
                }
            }
            td.insertTrainerToDb(trainer);
            System.out.println("Do you want to add more trainers?y/n");
            String moreTrainers = input.nextLine();
            if (!moreTrainers.equals("y")) {
                trainersOk = true;
            }
        }
    }

    public static void courseStudentRel(Course[] courses, Student[] students) {

        Scanner input = new Scanner(System.in);
        int cLength = courses.length;
        int sLength = students.length;

        boolean done = false;
        while (!done) {
            List<Integer> cIDs = new ArrayList();
            List<Integer> sIDs = new ArrayList();
            boolean inputOk = false;
            while (!inputOk) {
                int counter = 0;
                System.out.printf("%-20s%3s%4s %-12s %-12s %-8s%n", "Student Name", "sID", "cID", "Title", "Stream", "Type");
                while (counter < cLength || counter < sLength) {
                    if (counter < sLength) {
                        System.out.printf("%-20s%3d", students[counter].getFullName(), students[counter].getId());
                        sIDs.add(students[counter].getId());
                    } else {
                        System.out.printf("%23s", "");
                    }
                    if (counter < cLength) {
                        System.out.printf(" %-3d %-12s %-12s %-10s", courses[counter].getId(), courses[counter].getTitle(), courses[counter].getStream(), courses[counter].getType());
                        cIDs.add(courses[counter].getId());
                    }
                    System.out.println();

                    counter++;
                }

                System.out.println("Bind Student to Course using ids with dash between: [sID-cID]");
                String relation = input.nextLine().replaceAll(" ", "");

                int[] ids = Helper.checkIds(relation, sIDs, cIDs);
                if (ids[0] != -1) {

                    CourseStudentDao csd = new CourseStudentDao();
                    if (!csd.checkEntryIfExist(ids[0], ids[1])) {
                        if (csd.insertClassToDb(ids[0], ids[1])) {
                            System.out.println("Successfully inserted!");
                            inputOk = true;
                        } else {
                            System.out.println("Insertion failed");
                            break;
                        }
                    } else {
                        System.out.println("Entry already exist");
                        break;
                    }
                }
                System.out.println();
            }
            System.out.println("Do you want to add more relations?y/n");

            String moreRelations = input.nextLine();
            if (!moreRelations.equals("y")) {
                done = true;
            }

        }
    }

    public static void courseTrainerRel(Trainer[] trainers, Course[] courses) {

        Scanner input = new Scanner(System.in);
        int cLength = courses.length;
        int tLength = trainers.length;

        boolean done = false;
        while (!done) {
            List<Integer> cIDs = new ArrayList();
            List<Integer> tIDs = new ArrayList();
            boolean inputOk = false;
            while (!inputOk) {
                int counter = 0;
                System.out.printf("%-20s%3s%4s %-12s %-12s %-8s%n", "Trainer Name", "tID", "cID", "Title", "Stream", "Type");
                while (counter < cLength || counter < tLength) {
                    if (counter < tLength) {
                        System.out.printf("%-20s%3d", trainers[counter].getFullName(), trainers[counter].getId());
                        tIDs.add(trainers[counter].getId());
                    } else {
                        System.out.printf("%23s", "");
                    }
                    if (counter < cLength) {
                        System.out.printf(" %-3d %-12s %-12s %-10s", courses[counter].getId(), courses[counter].getTitle(), courses[counter].getStream(), courses[counter].getType());
                        cIDs.add(courses[counter].getId());
                    }
                    System.out.println();

                    counter++;
                }

                System.out.println("Bind Trainer to Course using ids with dash between: [tID-cID]");
                String relation = input.nextLine().replaceAll(" ", "");

                int[] ids = Helper.checkIds(relation, tIDs, cIDs);
                if (ids[0] != -1) {
                    CourseTrainerDao ctd = new CourseTrainerDao();
                    if (!ctd.checkEntryIfExist(ids[0], ids[1])) {

                        inputOk = false;
                        double payment = 0;
                        while (!inputOk) {
                            System.out.println("Give payment: ");
                            String payInput = input.nextLine().trim();
                            inputOk = Helper.checkDouble(payInput);
                            if (inputOk) {
                                payment = Double.parseDouble(payInput);
                            }
                        }
                        inputOk = false;
                        if (ctd.insertInstructToDb(ids[0], ids[1], payment)) {
                            System.out.println("Successfully inserted!");
                            inputOk = true;
                        } else {
                            System.out.println("Insertion failed");
                            break;
                        }
                    } else {
                        System.out.println("Entry already exist");
                        break;
                    }
                }
                System.out.println();
            }
            System.out.println("Do you want to add more relations?y/n");

            String moreRelations = input.nextLine();
            if (!moreRelations.equals("y")) {
                done = true;
            }

        }
    }

    public static void StudentAssignmentRel(Student[] students) {

        Scanner input = new Scanner(System.in);
        int sLength = students.length;
//        int aLength = assignments.length;

        boolean done = false;
        while (!done) {
            int sID = 0, aID = 0;
            List<Integer> sIDs = new ArrayList();
            boolean inputOk = false;
            while (!inputOk) {
                System.out.printf("%-20s%3s%n", "Student Name", "sID");
                for (Student s : students) {

                    System.out.printf("%-20s%3d%n", s.getFullName(), s.getId());
                    sIDs.add(s.getId());

                }

                System.out.println("Choose Student to relate with assignment by sId:");
                String sIdInput = input.nextLine().replaceAll(" ", "");

                inputOk = (Helper.checkInt(sIdInput) && sIDs.contains(Integer.parseInt(sIdInput)));
                if (inputOk) {
                    sID = Integer.parseInt(sIdInput);
                }
                else{
                    System.out.println("sID not available!");
                }

            }

            inputOk = false;
            while (!inputOk) {
                AssignmentDao ad = new AssignmentDao();
                Assignment[] assignments = ad.getAssignmentsAvailableForStudent(sID);

                List<Integer> aIDs = new ArrayList();
                System.out.printf("%-50s%-20s%3s%n", "Title", "SubDateTime", "aID");
                for (Assignment a : assignments) {
                    aIDs.add(a.getId());
                    System.out.printf("%-50s%-20s%3d%n", a.getTitle(),a.getSubDateTime().format(Helper.dateTimeFormat), a.getId());
                    sIDs.add(a.getId());

                }
                System.out.println("Choose Assignment by aId:");
                String aIdInput = input.nextLine().replaceAll(" ", "");

                inputOk = (Helper.checkInt(aIdInput) && aIDs.contains(Integer.parseInt(aIdInput)));
                if (inputOk) {
                    aID = Integer.parseInt(aIdInput);
                }
                else{
                    System.out.println("aID not available!");
                }

            }

            StudentAssignmentDao sad = new StudentAssignmentDao();
            if (!sad.checkEntryIfExist(sID, aID)) {

                inputOk = false;
                int oralMark = 0;
                while (!inputOk) {
                    System.out.println("Give Oral Mark: ");
                    String oralInput = input.nextLine().trim();
                    inputOk = Helper.checkInt(oralInput);
                    if (inputOk) {
                        oralMark = Integer.parseInt(oralInput);
                    }
                }
                inputOk = false;
                int totalMark = 0;
                while (!inputOk) {
                    System.out.println("Give Total Mark: ");
                    String totalInput = input.nextLine().trim();
                    inputOk = Helper.checkInt(totalInput);
                    if (inputOk) {
                        totalMark = Integer.parseInt(totalInput);
                    }
                }
                inputOk = false;
                if (sad.insertExamToDb(sID, aID, oralMark, totalMark)) {
                    System.out.println("Successfully inserted!");
                    inputOk = true;
                } else {
                    System.out.println("Insertion failed");
                    break;
                }
            } else {
                System.out.println("Entry already exist");
                break;
            }
            System.out.println(
                    "Do you want to add more relations?y/n");

            String moreRelations = input.nextLine();

            if (!moreRelations.equals(
                    "y")) {
                done = true;
            }
        }
        System.out.println();
    }

}
