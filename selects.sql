SELECT * FROM bc8individual.Courses;
SELECT * FROM bc8individual.Trainers;
SELECT * FROM bc8individual.Students;
SELECT * FROM bc8individual.Assignments;

-- All the students per course
SELECT * FROM bc8individual.Courses as c, bc8individual.Students as s, bc8individual.Classes as cl
	where cl.course_id=c.course_id and cl.student_id=s.student_id;

-- All the trainers per course
SELECT * FROM bc8individual.Courses as c, bc8individual.Trainers as t, bc8individual.Instructs as i
 WHERE c.course_id=i.course_id and i.trainer_id=t.trainer_id;

-- All the assignments per course
SELECT * FROM bc8individual.Courses as c, bc8individual.Assignments as a WHERE c.course_id=a.course_id;

-- All the assignments per course per student
SELECT * FROM bc8individual.Courses as c, bc8individual.Assignments as a, bc8individual.Students as s, bc8individual.Exams as e
	WHERE c.course_id=a.course_id and e.assignment_id=a.assignment_id and e.student_id=s.student_id;
    
-- A list of students that belong to more than one courses
SELECT s.student_id,s.last_name, s.first_name, COUNT(cl.student_id) as classSum FROM bc8individual.Classes as cl, bc8individual.Students as s
	WHERE cl.student_id=s.student_id 
    GROUP BY cl.student_id
    HAVING classSum>1;
    
