CREATE SCHEMA bc8individual DEFAULT CHARACTER SET utf8mb4 ;

CREATE TABLE bc8individual.Courses (
  course_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(45) NOT NULL,
  stream VARCHAR(45) NOT NULL,
  type VARCHAR(45) NOT NULL,
  start_date DATE NULL,
  end_date DATE NULL,
  fee DECIMAL(6,2) NULL);
  
CREATE TABLE bc8individual.Trainers (
  trainer_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  subject VARCHAR(45) NOT NULL);
  
CREATE TABLE bc8individual.Students (
  student_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(45) NOT NULL,
  last_name VARCHAR(45) NOT NULL,
  date_of_birth DATE NULL,
  tuition_fees DECIMAL(6,2) NULL);
  
CREATE TABLE bc8individual.Classes (
  class_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  course_id INT NOT NULL,
  student_id INT NOT NULL,
  UNIQUE KEY (course_id, student_id),
  CONSTRAINT FK_Classes_1 FOREIGN KEY (course_id) REFERENCES bc8individual.Courses (course_id),
  CONSTRAINT FK_Classes_2 FOREIGN KEY (student_id) REFERENCES bc8individual.Students (student_id));
  
CREATE TABLE bc8individual.Payments (
  payment_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  amount DECIMAL(6,2) NOT NULL,
  sub_date_time DATETIME NOT NULL,
  student_id INT NULL,
  CONSTRAINT FK_Payments_1 FOREIGN KEY (student_id) REFERENCES bc8individual.Students (student_id));
 
CREATE TABLE bc8individual.Assignments (
  assignment_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(45) NOT NULL,
  description VARCHAR(150) NOT NULL,
  sub_date_time DATETIME NOT NULL,
  course_id INT NULL,
  CONSTRAINT FK_Assignments_1 FOREIGN KEY (course_id) REFERENCES bc8individual.Courses (course_id));
  
CREATE TABLE bc8individual.Exams (
  exam_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  oral_mark INT NULL CHECK (oral_mark >=0 AND oral_mark <= 100),
  total_mark INT NULL CHECK (total_mark >=0 AND total_mark <= 100),
  student_id INT NOT NULL,
  assignment_id INT NOT NULL,
  CONSTRAINT FK_Exams_1 FOREIGN KEY (student_id) REFERENCES bc8individual.Students (student_id),
  CONSTRAINT FK_Exams_2 FOREIGN KEY (assignment_id) REFERENCES bc8individual.Assignments (assignment_id));

CREATE TABLE bc8individual.Instructs (
  instruct_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  payment DECIMAL(6,2) NULL,
  course_id INT NOT NULL,
  trainer_id INT NOT NULL,
  CONSTRAINT FK_Instructs_1 FOREIGN KEY (course_id) REFERENCES bc8individual.Courses (course_id),
  CONSTRAINT FK_Instructs_2 FOREIGN KEY (trainer_id) REFERENCES bc8individual.Trainers (trainer_id));

INSERT INTO bc8individual.Courses (title, stream, type, start_date, end_date, fee) VALUES ('BOOTCAMP 8', 'Full time', 'Java', '2019-07-21', '2019-11-21', '2100');
INSERT INTO bc8individual.Courses (title, stream, type, start_date, end_date, fee) VALUES ('BOOTCAMP 8', 'Part time', 'Java', '2019-07-21', '2020-01-21', '2100');
INSERT INTO bc8individual.Courses (title, stream, type, start_date, end_date, fee) VALUES ('BOOTCAMP 8', 'Full time', 'C#', '2019-07-21', '2019-11-21', '2100');
INSERT INTO bc8individual.Courses (title, stream, type, start_date, end_date, fee) VALUES ('BOOTCAMP 8', 'Part time', 'C#', '2019-07-21', '2020-01-21', '2100');

INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Full Time Individual Assignment - PART A', 'This is the first part of your Individual Project!', '2019-08-12', 1);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Full Time Individual Assignment - PART B', 'This is the second part of your Individual Project!', '2019-09-12', 1);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Full Time Individual Assignment - PART C', 'This is the third part of your Individual Project!', '2019-10-12', 1);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Full Time Individual Assignment - PART D', 'This is the forth part and final to your Individual Project!', '2019-11-18', 1);

INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Part Time Individual Assignment - PART A', 'This is the first part of your Individual Project!', '2019-08-28', 2);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Part Time Individual Assignment - PART B', 'This is the second part of your Individual Project!', '2019-10-14', 2);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Part Time Individual Assignment - PART C', 'This is the third part of your Individual Project!', '2019-12-27', 2);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('Java Part Time Individual Assignment - PART D', 'This is the forth part and final to your Individual Project!', '2020-01-18', 2);

INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Full Time Individual Assignment - PART A', 'This is the first part of your Individual Project!', '2019-08-12', 3);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Full Time Individual Assignment - PART B', 'This is the second part of your Individual Project!', '2019-09-12', 3);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Full Time Individual Assignment - PART C', 'This is the third part of your Individual Project!', '2019-10-12', 3);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Full Time Individual Assignment - PART D', 'This is the forth part and final to your Individual Project!', '2019-11-18', 3);

INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Part Time Individual Assignment - PART A', 'This is the first part of your Individual Project!', '2019-08-28', 4);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Part Time Individual Assignment - PART B', 'This is the second part of your Individual Project!', '2019-10-14', 4);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Part Time Individual Assignment - PART C', 'This is the third part of your Individual Project!', '2019-12-27', 4);
INSERT INTO bc8individual.Assignments (title, description, sub_date_time, course_id) VALUES ('C# Part Time Individual Assignment - PART D', 'This is the forth part and final to your Individual Project!', '2020-01-18', 4);

INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('James', 'White', 'Java');
INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('Abigail', 'Taylor', 'Java');
INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('Oliver', 'Smith', 'Java');
INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('Harry', 'Brown', 'C#');
INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('Emily', 'Roberts', 'C#');
INSERT INTO bc8individual.Trainers (first_name, last_name, subject) VALUES ('Emma', 'Miller', 'C#');

INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 1, 1);
INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 1, 2);
INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 2, 3);
INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 3, 4);
INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 3, 5);
INSERT INTO bc8individual.Instructs (payment, course_id, trainer_id) VALUES (1000, 4, 6);

INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Sophia', 'Wilson', '1988-09-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Jacob', 'Davis', '1992-01-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Michael', 'Nelson', '1986-10-10', '2100.00');

INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Amelia', 'Lopez', '1999-08-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Kyle', 'Wright', '2000-02-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Oscar', 'Scott', '1983-04-10', '2100.00');

INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Charlie', 'Flores', '1997-03-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Samantha', 'Cooper', '2001-07-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Megan', 'Thomson', '1984-12-10', '2100.00');

INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Thomas', 'Barnes', '1993-05-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Lily', 'Walker', '1994-02-10', '2100.00');
INSERT INTO bc8individual.Students (first_name, last_name, date_of_birth, tuition_fees) VALUES ('Ethan', 'Clark', '1982-12-10', '2100.00');

INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (1, 1);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (1, 2);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (1, 3);

INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (2, 4);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (2, 5);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (2, 6);

INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (3, 7);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (3, 8);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (3, 9);

INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (4, 10);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (4, 11);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (4, 12);

-- add second course for select
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (1, 12);
INSERT INTO bc8individual.Classes (course_id, student_id) VALUES (4, 1);

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 40, '1', '1');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 60, '1', '2');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 30, '1', '3');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '1', '4');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 40, '2', '1');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 60, '2', '2');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 30, '2', '3');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '2', '4');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 40, '3', '1');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 60, '3', '2');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 30, '3', '3');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '3', '4');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '4', '5');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '4', '6');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '4', '7');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '4', '8');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 40, '5', '5');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 60, '5', '6');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 30, '5', '7');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '5', '8');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 40, '6', '5');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 60, '6', '6');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 30, '6', '7');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '6', '8');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '7', '9');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '7', '10');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '7', '11');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '7', '12');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '8', '9');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '8', '10');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '8', '11');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '8', '12');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '9', '9');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '9', '10');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '9', '11');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '9', '12');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '10', '13');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '10', '14');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '10', '15');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '10', '16');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '11', '13');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '11', '14');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '11', '15');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '11', '16');

INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (80, 40, '12', '13');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (30, 60, '12', '14');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (40, 30, '12', '15');
INSERT INTO bc8individual.Exams (oral_mark, total_mark, student_id, assignment_id) VALUES (50, 80, '12', '16');